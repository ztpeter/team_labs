from pyblog import main, read, upload_from_stdin
from unittest.mock import patch
from unittest.mock import Mock
import os


def test_read_success():
    query = [{'date': '2021-06-23T16:12:52',
              'title': {'rendered': 'Pipeline-Pred UnitTest'},
              'content': {'rendered': '<p>Test</p>\n', 'protected': False}}]
    mock_response = Mock()
    mock_response.json.return_value = query
    title = read(mock_response)
    assert title == "{'rendered': 'Pipeline-Pred UnitTest'}"


def test_read_failure():
    query = [{'date': '2021-06-23T16:12:52',
              'title': {'rendered': 'Pipeline-Pred UnitTest'},
              'content': {'rendered': '<p>Test</p>\n', 'protected': False}}]
    mock_response = Mock()
    mock_response.json.return_value = query
    title = read(mock_response)
    assert title != "{'rendered': 'Pipeline-Predators FailureTest'}"


def test_upload_stdin_success():
    date = '2021-06-23T16:12:52'
    url = os.environ['WORDPRESS_URL']
    user = os.environ['WORDPRESS_USERNAME']
    password = os.environ['WORDPRESS_PASSWORD']
    response = upload_from_stdin(url, user, password, date)
    assert response == ""


def test_main_url_found():
    with patch('requests.get') as mock_request:
        mock_request.return_value.status_code = 200
        status = main()
        assert status == True  # noqa: E712


def test_main_url_not_found():
    with patch('requests.get') as mock_request:
        mock_request.return_value.status_code = 404
        status = main()
        assert status == False  # noqa: E712
