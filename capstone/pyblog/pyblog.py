import requests
from requests.auth import HTTPBasicAuth
import sys
import datetime
import os
import argparse


def main():
    try:
        getCurrentDateTime = datetime.datetime.now()
        date = (getCurrentDateTime.strftime("%Y-%m-%dT%H:%M:%S"))
        url = os.environ['WORDPRESS_URL']
        user = os.environ['WORDPRESS_USERNAME']
        password = os.environ['WORDPRESS_PASSWORD']
        request = requests.get(url, timeout=10)
        if request.status_code != 200:
            print("Wordpress Website is not available")
            return False
        else:
            response = requests.get(url, auth=HTTPBasicAuth(user, password))
            parser = argparse.ArgumentParser()
            parser.add_argument("field1")
            parser.add_argument("-f", type=str, dest="filename")
            args = parser.parse_args()
            if args.field1 == 'read':
                title = read(response)  # noqa: F841
            elif args.field1 == 'upload' and args.filename == '-':
                response = upload_from_stdin(url, user, password, date)
            elif args.field1 == 'upload' and args.filename != 'None':
                filename = args.filename
                upload_from_file(url, user, password, date, filename)
            else:
                print("Example Usage:")
                print("python3 pyblog.py read")
                print("- Outputs the contents of the latest blog post" +
                      " to stdout")
                print("python3 pyblog.py upload -f <filename>")
                print("- Uploads the contents of the specified file as a new"
                      + " blog post")
                print("python3 pyblog.py upload -f - < <filename>")
                print("- Uploads the contents using redirection of the" +
                      " specified file as a new blog post")
                print("")
    except requests.ConnectionError:
        print("Wordpress Website Cannot be reached")
    return True


def read(response):
    posts = 1
    postcount = 1
    for post in response.json():
        if postcount <= posts:
            title = str(post['title'])
            content = str(post['content'])
            print('Date:', post['date'], "\n" 'Title:', title[13:-1],
                  "\n" 'Content:', content[13:-21])
            postcount = postcount + 1
    return title


def upload_from_stdin(url, user, password, date):
    try:
        response = ''
        linecnt = 1
        contentlist = []
        for line in sys.stdin:
            if linecnt == 1:
                title = line.rstrip()
                linecnt += 1
            elif linecnt != 1 and line.rstrip() != 'exit':
                contentlist.append(line.rstrip())
                linecnt += 1
            elif line.rstrip() == 'exit':
                break
        content = '\n'.join([str(elem) for elem in contentlist])
        post = {
            'title': title,
            'status': 'publish',
            'content': content,
            'date': date
            }
        response = requests.post(url, auth=HTTPBasicAuth(user, password),
                                 json=post)
        if str(response) != "<Response [201]>":
            print("Post Failed, Please verify your credentials")
        else:
            print("Posted successfully")
    except OSError as error:
        print(error)
        print("File Not Exist")
    return response


def upload_from_file(url, user, password, date, filename):
    try:
        with open(filename, 'r') as f:
            lines = f.read()
            title = lines.split('\n', 1)[0]
            content = lines.split('\n', 1)[1]
        post = {
            'title': title,
            'status': 'publish',
            'content': content,
            'date': date
            }
        response = requests.post(url, auth=HTTPBasicAuth(user, password),
                                 json=post)
        if str(response) != "<Response [201]>":
            print("Post Failed, Please verify your credentials")
        else:
            print("Posted successfully")
    except IOError:
        print("File Not Exist")


if __name__ == '__main__':
    main()
