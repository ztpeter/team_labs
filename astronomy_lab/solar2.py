#!/usr/bin/env python3
import requests
import json
from datetime import datetime, timedelta, date, time
from requests.auth import HTTPBasicAuth
ASTRONOMYAPI_ID="53402084-257e-4003-9462-8e05d1529bb0"
ASTRONOMYAPI_SECRET="00a317853ef1de0c73d697be5c58a0a2478441df7c806b287dbeda18106324204be3a4d3fdeb38fbaa7d0aee6565620e899a2488d66afae926022a7ed057c45056705f9b8a1e23a795f27ee5969f60436634fbe0f9446c00d10704c258390d1efbd303e17a9ae169afaa018c3b3354d7"


def get_location():
    """Returns the longitude and latitude for the location of this machine.
    Returns:
    str: latitude
    str: longitude"""
    public_ip = requests.get('https://api.ipify.org').text
    #public_ip = input("Enter IP address: ")
    url = "http://ip-api.com/json/{IP}".format(IP=public_ip)
    response = requests.get(url)
    info = json.loads(response.text)
    latitude = str(info['lat'])
    longitude = str(info['lon'])

    location = str(info['city']) + ", " + str(info['region'])
    
    return latitude, longitude, location

def get_sun_position(latitude, longitude):
    """Returns the current position of the sun in the sky at the specified location
    Parameters:
    latitude (str)
    longitude (str)
    Returns:
    float: azimuth
    float: altitude
    """
    delta = timedelta(days=7)
    week_ago = datetime.now() - delta
    from_date = week_ago.strftime("%Y-%m-%d")
    today = datetime.today()
    to_date = today.strftime("%Y-%m-%d")
    now = datetime.now()
    time = now.strftime("%H:%M:%S")
    elevation = '34'
    sun_url = 'https://api.astronomyapi.com/api/v2/bodies/positions/sun'
    payload = {'latitude': latitude, 'longitude': longitude, 'elevation': elevation, 'from_date': from_date, 'to_date': to_date , 'time': time}
    sun_auth = requests.get(sun_url, auth=HTTPBasicAuth(ASTRONOMYAPI_ID, ASTRONOMYAPI_SECRET), params=payload)
    sun_info = json.loads(sun_auth.text)
    azimuth = sun_info['data']['table']['rows'][0]['cells'][1]['position']['horizonal']['azimuth']['string']
    altitude = sun_info['data']['table']['rows'][0]['cells'][1]['position']['horizonal']['altitude']['string']
    from_Earth = sun_info['data']['table']['rows'][0]['cells'][1]['distance']['fromEarth']['km']
    magnitude = sun_info['data']['table']['rows'][0]['cells'][1]['extraInfo']['magnitude']
    azimuth = azimuth.split(' ')
    azimuth = [sub[:-1] for sub in azimuth]
    azimuth = (azimuth[0], "deg", azimuth[1], "min", azimuth[2], "sec")
    altitude = altitude.split(' ')
    altitude = [sub[:-1] for sub in altitude]
    altitude = (altitude[0], "deg", altitude[1], "min", altitude[2], "sec")
    from_Earth = round(float(from_Earth))
    from_Earth = ("{:,}".format(from_Earth))
    magnitude = round(float(magnitude),2)
    
    return azimuth, altitude, from_Earth, magnitude

def print_position(azimuth, altitude, from_Earth, magnitude, location):
    """Prints the position of the sun in the sky using the supplied coordinates
    Parameters:
    azimuth (float)
    altitude (float)"""
    now= datetime.now()
    current_time = now.strftime("%I:%M %p")
    current_day = now.strftime("%B %d, %Y:")
    print("From", location, "at", current_time, "on", current_day)
    print("")
    print("Sun:")
    print("    Distance from Earth:", from_Earth, "km")
    print("    Magnitude:", magnitude)
    print("    Position:")
    print("        Azimuth:", " ".join(azimuth))
    print("        Altitude:", " ".join(altitude))
    
if __name__ == "__main__":
    latitude, longitude, location = get_location()
    azimuth, altitude, from_Earth, magnitude = get_sun_position(latitude, longitude)
    print_position(azimuth, altitude, from_Earth, magnitude, location)
