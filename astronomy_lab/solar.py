#!/usr/bin/env python3
import requests
import datetime
import json
from requests.auth import HTTPBasicAuth
ASTRONOMYAPI_ID = "d8b39f9a-cd14-4380-ac7c-729053a490f7"
ASTRONOMYAPI_SECRET = "009e7af933543ce94fb74a2f9a8306eba083116139d27765c25f4d5f03a8a3708ee04251766c028c5dbf6744b6e3911c4b654426eaec7d00a2edf0a13c22898136ae887d665d59fbc2ac9f1cf29efd134b3da6abb45fbe15c78f9813da9983306e32d438b123dd7470e7a466b99d9166"

def get_location():
    """ Returns the longitutude and latitude for the location of this machine
    Returns:
    str: latitude
    str: longitude"""
    ipadd = input("Enter ip address:")

    location = requests.get("http://ip-api.com/json/{ip}".format(ip=ipadd))
    json = location.json()
#    print(json)
    latitude = json['lat']
    longitude = json['lon']

    return latitude, longitude

def get_sun_position(latitude, longitude):
    """ Returns the current position of the sun in the sky in the specific location

    Parameters:
    latitude (str)
    longitude (str)
    
    Returns"
    float: azimuth
    float: altitude
    """

    elevation = "1000"
    from_date = "2021-04-01"
    to_date = "2021-04-10"
    current_time = "08:00:00"
    print(latitude)
    print(longitude)

    # BASIC CONNECTION
    sun_loc1 = requests.get('https://api.astronomyapi.com/api/v2/bodies', auth=HTTPBasicAuth(ASTRONOMYAPI_ID, ASTRONOMYAPI_SECRET))
    print(sun_loc1)


# Positions
    sunloc_url = "'https://api.astronomyapi.com/api/v2/bodies/positions/{lat}, {lon}, {elev}, {from_dt}, {to_dt}, {currtime}'.format(lat=latitude, lon=longitude, elev=elevation, from_dt=from_date, to_dt=to_date, currtime=current_time), auth=HTTPBasicAuth(ASTRONOMYAPI_ID, ASTRONOMYAPI_SECRET)"
    sunlocation_data = requests.get(sunloc_url).json()
    print(sunlocation_data) 

    return 0, 0

def print_position(azimuth, altitude):
    """ Prints the position of the sun in the sky using the supplied coordinates

    Parameters:
    azimuth (float)
    altitude (float)"""

    print("The Sun is currently at:")


if __name__ == "__main__":
    latitude, longitude = get_location()
    azimuth, altitude = get_sun_position(latitude, longitude)
#    print_position(azimuth, altitude)
    print("Latitude =", latitude, "Longitude = ", longitude)
